const http = require('http');
const https = require("https");
const WebSocketServer = require('websocket').server;

var options = {
  "method": "GET",
  "hostname": "numbersapi.p.rapidapi.com",
  "port": null,
  "path": "",
  "headers": {
    "x-rapidapi-host": "numbersapi.p.rapidapi.com",
    "x-rapidapi-key": "bc14f6cdf5msh29b7d81f0ee9ae8p13d2a3jsn4ff90239f1d6"
  }
};

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getTrivia(connection, type) {
  let month;
  let day;

  if (type === 'trivia') {
    options.path = "/random/trivia?fragment=true&json=true"
  } else if (type === 'math') {
    let number = getRandomInt(-20, 1000000000);
    options.path = `/${number}/math?fragment=false&json=true`
  } else if (type === 'year') {
    let year = getRandomInt(-3000, 2020);
    options.path = `/${year}/year?fragment=false&json=true`
  } else if (type === 'date') {
    month = getRandomInt(1, 12);
    day = getRandomInt(1, 31);
    options.path = `/${month}/${day}/date?fragment=false&json=true`
  }

  const req = https.request(options, function (res) {
    const chunks = [];

    res.on("data", function (chunk) {
      chunks.push(chunk);
    });

    res.on("end", function () {
      const body = Buffer.concat(chunks).toString();
      let bodyJson = JSON.parse(body);
      if (typeof month !== 'undefined') {
        bodyJson.day = day;
        bodyJson.month = month;
      }
      connection.send(JSON.stringify(bodyJson));
    });
  });

  req.end();
}

function localRepeater(connection) {
  connection.send("Just some random text!");
}

const server = http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'}); // http header
  var url = req.url;
  if (url === '/api/about') {
    res.write('<h1>about us page<h1>'); //write a response
    res.end(); //end the response
  } else if (url === '/api/contact') {
    res.write('<h1>contact us page<h1>'); //write a response
    res.end(); //end the response
  }
});
server.listen(9898);

const wsServer = new WebSocketServer({
  httpServer: server,
  path: "/websocket"
});

wsServer.on('request', function (request) {
  const connection = request.accept(null, request.origin);

  let triviaGenerator;

  connection.on('message', function (message) {
    let commandObject;
    try {
      commandObject = JSON.parse(message.utf8Data);
    } catch (e) {
      console.error("Cannot parse data.")
    }

    if (typeof commandObject !== 'undefined') {
      switch (commandObject.action) {
        case "close":
          connection.close();
          break;
        case "stop":
          if (triviaGenerator !== null) clearInterval(triviaGenerator);
          triviaGenerator = null;
          break;
        case "topic-switch":
          if (triviaGenerator !== null) clearInterval(triviaGenerator);
          triviaGenerator = null;
        case "start":
          if (triviaGenerator == null) {
            triviaGenerator = setInterval(getTrivia.bind(null, connection, commandObject.category), 5000);
          }
          break;
        default:
          console.log("Unknown command!");
      }
    }
  });

  connection.on('close', function (reasonCode, description) {
    console.log('Client has disconnected.');
  });
});