# rznu-lab2

Second assignment for Service Oriented Computing course at FER

### Lab 2.


The goal of the second lab is to implement a simple web service based on the Websocket protocol.
You can either choose to implement your own project or clone something popular from the web.
Central and most important part of the project has to be a real-time communication using the
Websocket protocol (chat application between client and server, chat application between two clients
or group of clients through the server, live notifications about sport results, etc.). Supporting
functions, such as user login, chat mate selection, or selection of sports event to be tracked, are
allowed to be exposed through the REST interface.
As part of this assignment, you should invest some time to research and analyze existing web servers
and development tools that, except the HTTP, support the Websocket protocol as well. Based on this
analysis, you should be able to choose those that satisfy the requirements of the project.
Front end (client-side part of the project) has to be implemented in the form of a web application and
has to be able to execute within a standard web browser that supports the Websocket protocol. None
of the web browser extensions or add-ons are allowed. Front end application has to implement
automatic detection of whether or not the given web browser supports the Websocket protocol, with
a warning massage to the user if her/his browser does not support it. The application has to be tested
with both versions of the web browser (one that supports the Websocket protocol and one that does
not).
Our expectations from students during the lab examination are to:
1. demonstrate the execution of the project on your own equipment (laptop) to the teaching
assistants, be familiar with the code, the tools being used, and the project in general, describe
how you have implemented specific parts of the project, introduce slight modifications in code
on request
2. demonstrate the real-time communication using the Websocket protocol
3. demonstrate the automatic detection of the Websocket protocol support in a web browser
(please, have both versions of the web browser prepared for demonstration)